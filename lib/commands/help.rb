# frozen_string_literal: true

module GFSM
  module Commands
    class Help < BaseCommand
      def run(_ = [])
        GFSM::Output.puts <<~HELP
        # GitLab Flavored Semantic Versioning

        Usage:

        gfsm <command> [<args>]

        Available commands:
        gfsm help               # Print this help text
        gfsm version            # Command used to print the current version or bump it based on the configuration
        gfsm changelog          # Command used to generate a changelog based on the commits since the latest release


        Version command
        #{GFSM::Commands::Version.help}
        Changelog command
        #{GFSM::Commands::Changelog.help}
        HELP

        true
      end
    end
  end
end
