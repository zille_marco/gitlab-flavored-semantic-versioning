# frozen_string_literal: true

module GFSM
  module Commands
    class Version < BaseCommand

      def self.help
        cli_info = GFSM::Tools::VersionBumperSettings.cli_info

        <<~HELP
        Usage:
        gfsm version [help|bump|current] #{cli_info[:usage]}

        Commands:
        help                                  # Prints this help
        bump                                  # Bump the current version based on the commits changelog trailer and the configuration file
        current                               # Print the current version, without bumping it

        Options: \n#{cli_info[:options]}

        Environment variables: \n#{cli_info[:environment_variables]}
        HELP
      end

      def run(args = [])
        case args.shift
        when 'bump'
          settings = GFSM::Tools::VersionBumperSettings.new(args)
          version_bumper = GFSM::Tools::VersionBumper.new(settings)
          version = version_bumper.execute

          GFSM::Output.puts(version)
        when 'current'
          settings = GFSM::Tools::VersionBumperSettings.new(args)
          version = GFSM::Tools::CurrentVersionLoader.load_current_version(settings.repository, settings.initial_version)

          if settings.prerelease
            version.add_prerelease_suffix!(settings.prerelease_name)
          end

          GFSM::Output.puts(version)
        when 'help'
          GFSM::Output.puts(GFSM::Commands::Version.help)
        else
          GFSM::Output.warn(GFSM::Commands::Version.help)
        end

        true
      end
    end
  end
end