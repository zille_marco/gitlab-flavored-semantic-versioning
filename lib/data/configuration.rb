# frozen_string_literal: true

require 'yaml'

module GFSM
  module Data
    class Configuration
      attr_reader :change_types

      def initialize(config_path)
        @change_types = []

        configuration = YAML.load_file(config_path)

        configuration.each do |key, value|
          if key == "change_types"
            self.load_change_types(value)
          end
        end

        @change_types
      end

      def get_change_type_from_category(category)
        @change_types.find do |change_type|
          category.match(change_type.matcher)
        end
      end

      private

      def load_change_types(change_types_config)
        change_types_config.each do |key, value|
          if !value.include?("matcher")
            raise "Change type #{key} is missing the matcher"
          end

          @change_types << GFSM::Data::ChangeType.new(
            /#{Regexp.quote(value["matcher"])}/i,
            value.include?("title") ? value["title"] : 'no-title',
            value.include?("bump") ? value["bump"].to_sym : :patch,
            value.include?("priority") ? value["priority"] : -100,
            value.include?("ignore_on_changelog") ? value["ignore_on_changelog"] : false,
          )
        end
      end
    end
  end
end