# frozen_string_literal: true

module GFSM
  module Data
    class ChangeType
      attr_reader :matcher, :title, :bump_type, :priority, :ignore_on_changelog, :commits

      def initialize(matcher, title, bump_type, priority, ignore_on_changelog)
        @matcher = matcher
        @title = title
        @bump_type = bump_type
        @priority = priority
        @ignore_on_changelog = ignore_on_changelog
        @commits = []
      end

      def bump_major?
        @bump_type == :major
      end
      
      def bump_minor?
        @bump_type == :minor
      end
      
      def bump_patch?
        @bump_type == :patch
      end

      def ignore_on_changelog?
        @ignore_on_changelog
      end

      def to_s
        @title
      end

      def to_changelog_entry
        "### #{@title}"
      end

      def self.find_highest_bump(change_types)
        highest_bump = :patch

        change_types.each do |change_type|
          if change_type.bump_major?
            highest_bump = :major

            # Early exit, it can't go higher than this!
            break
          elsif change_type.bump_minor?
            highest_bump = :minor
          end
        end

        highest_bump
      end
    end
  end
end