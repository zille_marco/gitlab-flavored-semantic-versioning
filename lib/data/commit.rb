# frozen_string_literal: true

require 'forwardable'

module GFSM
  module Data
    class Commit
      extend Forwardable

      attr_reader :category, :trailer_key, :subject

      def initialize(commit, trailer_key, category)
        @commit = commit
        @trailer_key = trailer_key
        @category = category
        @subject = commit.message.lines.first.chomp
      end

      def short_sha(length = 7)
        self.sha[0, length]
      end

      def to_changelog_entry
        "#{subject} (#{short_sha})"
      end

      delegate %i[message sha] => :@commit
    end
  end
end