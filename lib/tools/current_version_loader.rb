# frozen_string_literal: true

require 'git'
require_relative '../data/version'

module GFSM
  module Tools
    class CurrentVersionLoader
      def self.load_current_version(repo, initial_version)
        last_tag_name = GFSM::Tools::GitUtilities.extract_last_tag_name(repo)

        return GFSM::Data::Version.new(initial_version) unless last_tag_name

        if last_tag_name.downcase.start_with?("v") 
          last_tag_name.slice!(0)
        end

        GFSM::Data::Version.new(last_tag_name)
      end
    end
  end
end