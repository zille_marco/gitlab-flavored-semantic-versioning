FROM ruby:3.2.4-alpine3.20

ARG GFSM_VERSION

RUN apk add git
RUN gem install gfsm -v ${GFSM_VERSION}

COPY ./gfsmrc.yml ./gfsmrc.yml

ENTRYPOINT [""]
